from django.contrib import admin
from .models import Department, JobPost, ApplicantDetail

# Register your models here.

@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['id', 'dept_name']


@admin.register(JobPost)
class JobPostAdmin(admin.ModelAdmin):
    list_display = ['id', 'position', 'location', 'qualifications', 'responsibility', 'salary', 'department']


@admin.register(ApplicantDetail)
class ApplicantDetailAdmin(admin.ModelAdmin):
    list_display = ['id', 'fname', 'lname', 'email', 'linked_in', 'git_hub', 'department', 'jobpost', 'resume']