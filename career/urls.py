from django.urls import path
from . import views

urlpatterns = [
    path('career/', views.dept_info, name="dept"),
    path('position/<int:pk>/', views.position_info, name="position"),
    path('details/<str:dp>/<str:pos>/', views.form_details, name="dtls"),
    path('thankyou/', views.thank_you, name="thanks"),
]
