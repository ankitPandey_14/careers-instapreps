from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.

class Department(models.Model):
    dept_name = models.CharField(max_length=100)

    def __str__(self):
        return self.dept_name


class JobPost(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    position = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    qualifications = models.CharField(max_length=1000, help_text="Please mention the qualification in points(numerical)")
    responsibility = models.CharField(max_length=1000, help_text="Please mention the responsibilities in points(numerical)")
    salary = models.CharField(max_length=500)

    def __str__(self):
        return self.position


class ApplicantDetail(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    jobpost = models.ForeignKey(JobPost, on_delete=CASCADE)
    fname = models.CharField(max_length=68)
    lname = models.CharField(max_length=68)
    email = models.EmailField(max_length=200)
    linked_in = models.URLField(max_length=500, blank=True)
    git_hub = models.URLField(max_length=500, blank=True)
    resume = models.FileField(upload_to='doc', help_text="Only upload .pdf, .docx")