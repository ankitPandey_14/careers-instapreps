from django.core import validators
from django import forms
from .models import ApplicantDetail

class ApplicantRegistration(forms.ModelForm):
    class Meta:
        model = ApplicantDetail
        fields = ['fname', 'lname', 'email', 'linked_in', 'git_hub', 'resume']
        labels = {
            'fname':'First Name *',
            'lname':'Last Name *',
            'email':'Email Id *',
            'linked_in':'Linked In',
            'git_hub':'Github Link',
            'resume':'Upload Resume *'
        }
        help_text = {
            'resume': "Only upload .pdf, .docx",
        }