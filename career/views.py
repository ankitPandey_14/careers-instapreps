from django.shortcuts import render, HttpResponseRedirect
from django.contrib import messages
from .models import Department, JobPost, ApplicantDetail
from .forms import ApplicantRegistration

# Create your views here.

def dept_info(request):
    dpt = Department.objects.all()
    return render(request, 'career/department.html', {'dpt':dpt})


def position_info(request, pk):
    jb_p = JobPost.objects.filter(department=pk)
    return render(request, 'career/jobs.html', {'jb_p': jb_p})


def form_details(request, dp, pos):
    p = Department.objects.get(dept_name=dp)
    jb = JobPost.objects.get(position=pos)
    if request.method == 'POST':
        fm = ApplicantRegistration(request.POST, request.FILES)
        if fm.is_valid():
            fn = fm.cleaned_data['fname']
            ln = fm.cleaned_data['lname']
            em = fm.cleaned_data['email']
            lin = fm.cleaned_data['linked_in']
            git = fm.cleaned_data['git_hub']
            rsm = fm.cleaned_data['resume']
            reg = ApplicantDetail(department=p, jobpost=jb, fname=fn, lname=ln, email=em, linked_in=lin, git_hub=git, resume=rsm)
            reg.save()
            messages.success(request, f"Your apllication has been submitted.\nThank you for applying for {pos} post.\nGOOD LUCK !!")
            return HttpResponseRedirect('/thankyou/')

    else:
        fm = ApplicantRegistration()
    return render(request, 'career/apply.html', {'form':fm, 'pos':pos})


def thank_you(request):
    return render(request, 'career/thankyou.html')