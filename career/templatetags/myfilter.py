from django import template

register = template.Library()

@register.filter(name="split_String")
def split_strs(value, arg):
    lst = [ls for ls in value.split(arg)]
    return lst