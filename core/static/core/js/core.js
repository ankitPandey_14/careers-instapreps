document.getElementById("menu-click").addEventListener("click", menuToggle);

function menuToggle() {
    let menuClickBar = document.getElementById("menu-click-bar");
    menuClickBar.classList.toggle('active');
}